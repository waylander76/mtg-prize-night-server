const { generateSessionId } = require("../../utils/generateSessionId");

const sessions = [];

function get(req, res) {
  return res.send(JSON.stringify(sessions));
}

function getById(req, res) {
  const sessionIdx = sessions.findIndex((s) => s.sessionId == req.params.id);
  return res.send(JSON.stringify(sessions[sessionIdx]));
}

function create(req, res) {
  const generatedSessionId = generateSessionId(8);
  sessions.push({
    sessionId: generatedSessionId,
    state: {},
  });
  setTimeout(() => {
    try {
      removeSession(generateSessionId);
    } catch {
      console.log(`couldnt find a session with an id ${generatedSessionId}`);
    }
  }, 43200000); // 12h
  return res.send(
    `created a session ${sessions[sessions.length - 1].sessionId}`
  );
}

function update(req, res) {
  const sessionIdx = sessions.findIndex((s) => s.sessionId == req.params.id);
  sessions[sessionIdx].state = req.body;
  return res.send(
    `new state of session ${req.params.id} is: \n${JSON.stringify(sessions)}`
  );
}

function remove(req, res) {
  removeSession(req.params.id);
  return res.send(`removed session ${req.params.id}`);
}

function removeSession(id) {
  const sessionIdx = sessions.findIndex((s) => s.sessionId == id);
  sessions.splice(sessionIdx, 1);
}

module.exports = {
  get,
  getById,
  create,
  remove,
  update,
};
