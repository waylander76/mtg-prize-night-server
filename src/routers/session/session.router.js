const express = require("express");
const router = express.Router();
const sessionsController = require("../../controllers/session/sessionController");

router.get("/", sessionsController.get);

router.get("/:id", sessionsController.getById);

router.post("/", sessionsController.create);

router.put("/:id", sessionsController.update);

router.delete("/:id", sessionsController.remove);

module.exports = router;
