const { WebSocket, WebSocketServer } = require("ws");
const http = require("http");
const uuidv4 = require("uuid").v4;
const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");

const sessionsRouter = require("./src/routers/session/session.router");

const PORT = 3001;

const app = express();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cors());

app.use("/create-session", sessionsRouter);

const server = http.createServer(app);
const wsServer = new WebSocketServer({ server });
server.listen(PORT, () => {
  console.log(`WebSocket server is running on port ${PORT}`);
});

// maintaining all active connections in this object
const clients = {};
// maintaining all active users in this object
const users = {};
// Event types
const typesDef = {
  USER_EVENT: "userevent",
  CONTENT_CHANGE: "contentchange",
};

function broadcastMessage(json) {
  const data = JSON.stringify(json);
  if (json.type === typesDef.CONTENT_CHANGE) {
    const sentFrom = json.data.userId;
    const sentFromSessionId = users[sentFrom].username.split(":")[0];
    for (let userId in clients) {
      const client = clients[userId];
      const user = users[userId];
      const userSessionId = user.username.split(":")[0];
      if (
        client.readyState === WebSocket.OPEN &&
        sentFromSessionId === userSessionId
      ) {
        client.send(data);
      }
    }
  } else {
    for (let userId in clients) {
      let client = clients[userId];
      if (client.readyState === WebSocket.OPEN) {
        client.send(data);
      }
    }
  }
}

function handleMessage(message, userId) {
  const dataFromClient = JSON.parse(message.toString());
  const json = { type: dataFromClient.type };

  if (dataFromClient.type === typesDef.USER_EVENT) {
    users[userId] = dataFromClient;
    json.data = { users };
  } else if (dataFromClient.type === typesDef.CONTENT_CHANGE) {
    json.data = { userId, eventType: "UPDATE" };
  }

  broadcastMessage(json);
}

function handleDisconnect(userId) {
  console.log(`${userId} disconnected.`);
  const json = { type: typesDef.USER_EVENT };
  json.data = { users };
  delete clients[userId];
  delete users[userId];
  broadcastMessage(json);
}

wsServer.on("connection", function (connection) {
  const userId = uuidv4();
  console.log("Receieved a new connection");

  clients[userId] = connection;
  console.log(`${userId} connected.`);
  connection.on("message", (message) => handleMessage(message, userId));

  connection.on("close", () => handleDisconnect(userId));
});
