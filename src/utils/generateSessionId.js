function randomChar() {
  const index = Math.floor(Math.random() * 62);
  if (index < 10) {
    return String(index);
  } else if (index < 36) {
    return String.fromCharCode(index + 55);
  } else {
    return String.fromCharCode(index + 61);
  }
}

function generateSessionId(length) {
  let result = "";
  while (length > 0) {
    result += randomChar();
    length--;
  }
  return result;
}

module.exports = {
  generateSessionId,
};
